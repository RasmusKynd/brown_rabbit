/* Search & Highlight */
  $(document).ready(function(){
    $("#search").keyup(function(){
      $(".context").unmark();
       $(".context").mark($(this).val(), {"exclude": ["button"]});
    });
  });

var slideIndex = Math.floor(Math.random() * 6) + 1;
$(document).ready(showSlides);

function showSlides(n) {
var i;
var slides = document.getElementsByClassName("mySlides");
if (n > slides.length) {slideIndex = 1}
if (n < 1) {slideIndex = slides.length}
for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
}
slides[slideIndex-1].style.display = "block";
}

// Next/previous controls
function plusSlides(n) {
showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
showSlides(slideIndex = n);
}


/* ReadMore */

function readMore1() {
  var x = document.getElementById("DIV1");
  if (x.style.display === "none") {
    x.style.display = "block";
    $("#btn1").html('Read less');
  } else {
    x.style.display = "none";
    $("#btn1").html('Read more');
  }
}

function readMore2() {
  var x = document.getElementById("DIV2");
  if (x.style.display === "none") {
    x.style.display = "block";
    $("#btn2").html('Read less');
  } else {
    x.style.display = "none";
    $("#btn2").html('Read more');
  }
}

function readMore3() {
  var x = document.getElementById("DIV3");
  if (x.style.display === "none") {
    x.style.display = "block";
    $("#btn3").html('Read less');
  } else {
    x.style.display = "none";
    $("#btn3").html('Read more');
  }
}
